﻿!  VP_01.2.f90 
!
!  FUNCTIONS:
!  Производит перемножение трехдиагональных матриц из файла и выводит в файл
!

!****************************************************************************
!
!  PROGRAM: VP_012
!
!  PURPOSE:  Чтение и вывод матриц
!
!****************************************************************************

    program VP_012
    use MatrixMult
    implicit none
    real(8), allocatable, dimension(:,:) :: A, B, C
    integer(4) :: n, i

    !Нам сказано, что исходные матрицы не влезут в оперативную память целиком, но вот их ненулевые элементы - вполне.
    !Вырежем же эти элементы и составим из них новую матрицу nх3. В этой новой матрице не хватает первого и последнего элемента. Притворимся,
    !что они есть, но равны нулю.
    
    open(1, file='data1.dat')
        read(1,'(2x,I6)') n
        allocate(A(n,3))
        read(1,*)A(1,2:3)
        A(1,1)=0
            do i=2,n-1
                read(1,*)A(i,1:3)
            enddo
        read(1,*)A(n,1:2)
        A(n,3)=0
    close(1)
    
    open(2, file='data2.dat')
        read(2,'(2x,I6)') n
        allocate(B(n,3))
        read(2,*)B(1,2:3)
        B(1,1)=0
            do i=2,n-1
                read(2,*)B(i,1:3)
            enddo
        read(2,*)B(n,1:2)
        B(n,3)=0
    close(2)
    
    allocate(C(n,5))
    C=0
    
    call TriMatrixMultiplication(A, B, C, n)
    
    open(3, file='result.dat')
        write(3,"('# ',I6)") n
        write(3,*)C(1,3:5)
        write(3,*)C(2,2:5)
            do i=3,n-2
                write(3,*)C(i,1:5)
            enddo
        write(3,*)C(n-1,1:4)
        write(3,*)C(n,1:3)   
    close(3)


    deallocate(A)
    deallocate(B)
    deallocate(C)

    
    end program VP_012

