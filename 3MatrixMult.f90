﻿
!****************************************************************************
!
!  PROGRAM: VP_011
!
!  PURPOSE: Модуль, перемножающий квадратные трехдиагональные матрицы
!
!****************************************************************************
    
module MatrixMult
  use omp_lib
  implicit none
  contains

    subroutine TriMatrixMultiplication(A, B, C, n)
    implicit none
    real(8), intent(in), dimension(:,:) :: A, B
    real(8), dimension(:,:) :: C
    integer(4) :: n
    
 !искомая матрица пятидиагональная. Из ее ненулевых элементов получается матрица nх5. Каждую строчку этой матрицы придется считать отдельно
        call FindRow1(A, B, C, n)
        call FindRow2(A, B, C, n)
        call FindRow3(A, B, C, n)
        call FindRow4(A, B, C, n)
        call FindRow5(A, B, C, n)
        
    end subroutine TriMatrixMultiplication
    
    subroutine FindRow1(A, B, C, n)
    implicit none
    real(8), intent(in), dimension(:,:) :: A, B
    real(8), dimension(:,:) :: C
    integer(4) :: n, i
    
    !$omp parallel shared(A, B, C), private(i)
    !$omp do schedule(static)
        do i=3,n
            C(i,1)=A(i,1)*B(i-1,1)
        enddo
    !$omp end parallel    
    end subroutine FindRow1
    
    subroutine FindRow2(A, B, C, n)
    implicit none
    real(8), intent(in), dimension(:,:) :: A, B
    real(8), dimension(:,:) :: C
    integer(4) :: n, i
    
    !$omp parallel shared(A, B, C), private(i)
    !$omp do schedule(static)    
        do i=2,n
            C(i,2)=A(i,1)*B(i-1,2)+A(i,2)*B(i,1)
        enddo
    !$omp end parallel    
    end subroutine FindRow2

    subroutine FindRow3(A, B, C, n)
    implicit none
    real(8), intent(in), dimension(:,:) :: A, B
    real(8), dimension(:,:) :: C
    integer(4) :: n, i

    !$omp parallel shared(A, B, C), private(i)    
        !$omp single
            C(1,3)=A(1,2)*B(1,2)+A(1,3)*B(2,1)
        !$omp end single

        !$omp do schedule(static)            
        do i=2,n-1
            C(i,3)=A(i,1)*B(i-1,3)+A(i,2)*B(i,2)+A(i,3)*B(i+1,1)
        enddo
        
        !$omp single
            C(n,3)=A(n,1)*B(n-1,3)+A(n,2)*B(n,2)
        !$omp end single    
    !$omp end parallel
        
    end subroutine FindRow3
    
    subroutine FindRow4(A, B, C, n)
    implicit none
    real(8), intent(in), dimension(:,:) :: A, B
    real(8), dimension(:,:) :: C
    integer(4) :: n, i
 
    !$omp parallel shared(A, B, C), private(i)
    !$omp do schedule(static)    
        do i=1,n-2
            C(i,4)=A(i,2)*B(i,3)+A(i,3)*B(i+1,2)
        enddo
        
        !$omp single
            C(n-1,4)=A(n-1,2)*B(n-1,3)+A(n-1,3)*B(n,2)
        !$omp end single    
    !$omp end parallel
        
    end subroutine FindRow4
    
    subroutine FindRow5(A, B, C, n)
    implicit none
    real(8), intent(in), dimension(:,:) :: A, B
    real(8), dimension(:,:) :: C
    integer(4) :: n, i
 
    !$omp parallel shared(A, B, C), private(i)
    !$omp do schedule(static)    
        do i=1,n-2
            C(i,5)=A(i,3)*B(i+1,3)
        enddo 
    !$omp end parallel
        
    end subroutine FindRow5

end module MatrixMult    