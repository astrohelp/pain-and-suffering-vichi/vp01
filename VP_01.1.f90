﻿!  VP_01.1.f90 
!
!  FUNCTIONS:
!  Производит перемножение квадратных матриц из файла и выводит в файл
!

!****************************************************************************
!
!  PROGRAM: VP_011
!
!  PURPOSE:  Чтение и вывод матриц
!
!****************************************************************************

    program VP_011
    use MatrixMult
    implicit none
    real(8), allocatable, dimension(:,:) :: A, B, C
    integer(4) :: n, i

    open(1, file='data1.dat')
        read(1,'(2x,I6)') n
        allocate(A(n,n))
        read(1,*)A
    close(1)

    open(2, file='data2.dat')
        read(2,*)
        allocate(B(n,n))
        read(2,*)B
    close(1)

    allocate(C(n,n))
    C=0
    
    call MatrixMultiplication(A, B, C, n)
    
    open(3, file='result.dat')
        write(3,"('# ',I6)") n
        do i=1,n
            write(3,*)C(i,:)
        enddo
    close(3)


    deallocate(A)
    deallocate(B)
    deallocate(C)

    end program VP_011