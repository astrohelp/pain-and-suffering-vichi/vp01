﻿
!****************************************************************************
!
!  PROGRAM: VP_011
!
!  PURPOSE: Модуль, перемножающий квадратные матрицы
!
!****************************************************************************
    
module MatrixMult
  use omp_lib
  implicit none
  contains

    subroutine MatrixMultiplication(A, B, C, n)
    implicit none
    real(8), intent(in), dimension(:,:) :: A, B
    real(8), allocatable, dimension(:,:) :: C_tran
    real(8), dimension(:,:) :: C
    integer(4) :: n, i, j, k     
    
    allocate(C_tran(n,n))
    
    !$omp parallel shared(A, B, C_tran), private(i,j,k)
    !$omp do schedule(static)
        do i=1, n, 1
            do j=1, n, 1
                do k=1, n, 1
                    c_tran(i,j) = c_tran(i,j) + B(i,k) * A(k,j)
                enddo
            enddo
        enddo
    !$omp end parallel   
    c=transpose(c_tran)
    end subroutine MatrixMultiplication

end module MatrixMult